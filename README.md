[![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.alexbarter/java-lib/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.alexbarter/java-lib)
[![pipeline status](https://gitlab.com/percivalalb/java-lib/badges/master/pipeline.svg)](https://gitlab.com/percivalalb/java-lib/-/commits/master)
[![coverage report](https://gitlab.com/percivalalb/java-lib/badges/master/coverage.svg)](https://gitlab.com/percivalalb/java-lib/-/commits/master)

# java-lib
 Java boilerplate code

# Use in your gradle project

```
repositories {
    mavenCentral()
}
```

```
dependencies {
    implementation 'com.alexbarter:java-lib:0.2.0'
}
```
