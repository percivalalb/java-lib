/**
 * Java Library - Useful utility function
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package com.alexbarter.math;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;

import static com.alexbarter.math.ComplexNumber.real;
import static com.alexbarter.math.ComplexNumber.imag;
import static com.alexbarter.math.ComplexNumber.fromCart;
import static com.alexbarter.math.ComplexNumber.fromPolar;
import static com.alexbarter.math.ComplexNumber.i;
import static com.alexbarter.math.ComplexNumber.e;
import static com.alexbarter.math.ComplexNumber.PI;
import static com.alexbarter.math.ComplexNumber.ONE;

import org.junit.jupiter.api.Test;

/**
 * @author Alex
 *
 */
public class ComplexNumberTest {


    @Test
    public void creation() {
        assertThrows(IllegalArgumentException.class, () -> fromPolar(-1, 0));
        assertDoesNotThrow(() -> fromPolar(0, 0));
        assertDoesNotThrow(() -> fromCart(0, 0));
    }

    @Test
    public void log() {
        assertEquals(fromCart(Math.log(136) / 2D, Math.atan(5D/3D)), fromCart(6, 10).log());
        assertEquals(fromCart(Math.log(58) / 2D, Math.PI - Math.atan(7D/3D)), fromCart(-3, 7).log());
    }

    @Test
    public void power() {
        assertEquals(fromCart(-1584, 80), fromCart(6, 10).power(3));
        assertEquals(fromCart(-40, -42), fromCart(-3, 7).power(2));

        assertEquals(e.power(PI.divide(-2).getRealPart()), i.power(i));
    }

    @Test
    public void add() {
        assertEquals(fromCart(-26, 12), fromCart(6, 10).add(fromCart(-32, 2)));
        assertEquals(fromCart(-15, 23), fromCart(-90, 0).add(fromCart(75, 23)));
    }

    @Test
    public void subtract() {
        assertEquals(fromCart(38, 8), fromCart(6, 10).subtract(fromCart(-32, 2)));
        assertEquals(fromCart(-165, -23), fromCart(-90, 0).subtract(fromCart(75, 23)));
    }

    @Test
    public void multiply() {
        assertEquals(fromCart(-212, -308), fromCart(6, 10).multiply(fromCart(-32, 2)));
        assertEquals(fromCart(-6750, -2070), fromCart(-90, 0).multiply(fromCart(75, 23)));
    }

    @Test
    public void divide() {
        assertEquals(fromCart(-43D/257D, -83D/257D), fromCart(6, 10).divide(fromCart(-32, 2)));
        assertEquals(fromCart(-3375D/3077D, 1035D/3077D), fromCart(-90, 0).divide(fromCart(75, 23)));
    }

    @Test
    public void inverse() {
        assertEquals(ONE, fromCart(6, 10).inverse().multiply(fromCart(6, 10)));
        assertEquals(ONE, fromCart(-90, 0).inverse().multiply(fromCart(-90, 0)));
    }

    @Test
    public void nthRoots() {
        assertEquals(Arrays.asList(ONE, i, ONE.minus(), i.minus()), ONE.nthRoots(4));
        assertEquals(Arrays.asList(fromPolar(1, Math.PI / 6), fromPolar(1, 5 * Math.PI / 6), i.minus()), i.nthRoots(3));

        assertEquals(Arrays.asList(real(Math.sqrt(Math.E)), real(-Math.sqrt(Math.E))), e.nthRoots(2));
    }


}
