/**
 * Java Library - Useful utility function
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package com.alexbarter.math;

import static com.alexbarter.math.ComplexNumber.ZERO;
import static com.alexbarter.math.ComplexNumber.ONE;
import static com.alexbarter.math.ComplexNumber.real;
import static com.alexbarter.math.ComplexNumber.fromCart;
import static com.alexbarter.math.ComplexNumber.fromPolar;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class ComplexPolynomialTest {

    @Test
    public void laurentSeries() {
        ComplexPolynomial poly = new ComplexPolynomial();
        poly.setNegative(150, (k) -> fromCart(-1/4D, 0));
        poly.setNonNegative(150, (k) -> fromCart(-1/(4D*Math.pow(5, k+1)), 0));

        assertEquals(fromCart(-1/3D, 0), poly.evaluate(fromCart(4, 0)));
    }

    @Test
    public void taylorSeries() {
        ComplexPolynomial poly = new ComplexPolynomial();
        poly.setNonNegative(150, (k) -> fromCart(1, 0));

        assertEquals(fromCart(2, 0), poly.evaluate(fromCart(0.5D, 0)));
    }

    @Test
    public void multiplying() {
        ComplexPolynomial poly1 = new ComplexPolynomial();
        ComplexPolynomial poly2 = new ComplexPolynomial();
        poly1.setCoefficent(1, real(5), ZERO, real(3));
        poly2.setCoefficent(-10, ONE, real(7), real(2));

        ComplexPolynomial poly3 = new ComplexPolynomial();
        poly3.setCoefficent(-9, real(5), real(35), real(13), real(21), real(6));

        //assertEquals(poly3.evaluate(fromCart(2, 0)), poly1.multiply(poly2).evaluate(fromCart(2, 0)));

        assertEquals(poly3, poly1.multiply(poly2));
    }
}
