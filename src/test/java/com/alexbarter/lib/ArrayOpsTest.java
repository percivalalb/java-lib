package com.alexbarter.lib;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static com.alexbarter.lib.ArrayOps.*;

public class ArrayOpsTest {

    @Test
    public void testMap() {
        assertArrayEquals(new Integer[] {5,2,7,8}, map(v -> Integer.valueOf(v + 1), new Integer[] {4,1,6,7}));
        assertArrayEquals(new Integer[0], map(v -> null, new Integer[0]));
        assertArrayEquals(new Integer[] {null, null}, map(v -> null, new Integer[] {1,1}));
    }

    @Test
    public void testMapBi() {
        // These are the same tests as inFilter
        assertArrayEquals(new Integer[] {5,2,7,8}, map((v, i) -> Integer.valueOf(v + 1), new Integer[] {4,1,6,7}));
        assertArrayEquals(new Integer[0], map((v, i) -> null, new Integer[0]));
        assertArrayEquals(new Integer[] {null, null}, map((v, i) -> null, new Integer[] {1,1}));

        assertArrayEquals(new Integer[] {0, 1, 4}, map((v, i) -> i * i, new Integer[] {1,3,2}));
        assertArrayEquals(new Integer[] {1, 4, 6}, map((v, i) -> i * i + v, new Integer[] {1,3,2}));
    }

    @Test
    public void testFilter() {
        // Filters to non-negative numbers
        assertArrayEquals(new Integer[] {4,6}, filter(v -> v >= 0, new Integer[] {4,-1,6,-7}));
        assertArrayEquals(new Integer[0], filter(v -> false, new Integer[] {1,2}));
        assertArrayEquals(new Integer[] {1,2}, filter(v -> true, new Integer[] {1,2}));
    }

    @Test
    public void testFilterBi() {
        // These are the same tests as inFilter
        assertArrayEquals(new Integer[] {4,6}, filter((v, i) -> v >= 0, new Integer[] {4,-1,6,-7}));
        assertArrayEquals(new Integer[0], filter((v, i) -> false, new Integer[] {1,2}));
        assertArrayEquals(new Integer[] {1,2}, filter((v, i) -> true, new Integer[] {1,2}));

        assertArrayEquals(new Integer[] {4,6,-7}, filter((v, i) -> v >= 0 || i >= 2, new Integer[] {4,-1,6,-7}));
    }

    @Test
    public void testFirst() {
        // Filters to non-negative numbers
        Integer[] t = {1,2,3,4,5};
        Integer[][] rs = {{},{1},{1,2},{1,2,3},{1,2,3,4},{1,2,3,4,5},{1,2,3,4,5}};

        for (int n = 0; n < rs.length; n++) {
            assertArrayEquals(rs[n], first(t, n));
        }
    }

    @Test
    public void testLast() {
        // Filters to non-negative numbers
        Integer[] t = {1,2,3,4,5};
        Integer[][] rs = {{},{5},{4,5},{3,4,5},{2,3,4,5},{1,2,3,4,5},{1,2,3,4,5}};

        for (int n = 0; n < rs.length; n++) {
            assertArrayEquals(rs[n], last(t, n));
        }
    }

    @Test
    @Disabled
    public void testPrimitive() {
        // TODO: Add primitive function
//        assertArrayEquals(new byte[] {1}, primitive(new Byte[] {1}));
//        assertArrayEquals(new short[] {1}, primitive(new Short[] {1}));
//        assertArrayEquals(new int[] {1}, primitive(new Integer[] {1}));
//        assertArrayEquals(new long[] {1}, primitive(new Long[] {1L}));
//        assertArrayEquals(new float[] {1}, primitive(new Float[] {1F}));
//        assertArrayEquals(new double[] {1}, primitive(new Double[] {1D}));
//        assertArrayEquals(new char[] {'A'}, primitive(new Character[] {'A'}));
    }
}
