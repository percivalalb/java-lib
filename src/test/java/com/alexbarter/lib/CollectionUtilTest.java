/**
 * Java Library - Useful utility function
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package com.alexbarter.lib;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class CollectionUtilTest {

    @Test
    public void testAllMatch() {
        assertTrue(CollectionUtil.allMatch(Arrays.asList(4,6,10,16,20), (v) -> v % 2 == 0));
        assertFalse(CollectionUtil.allMatch(Arrays.asList(2,1), (v) -> v % 2 == 0));

        assertTrue(CollectionUtil.allMatch(Arrays.asList(), (v) -> false));
        assertTrue(CollectionUtil.allMatch(Arrays.asList(), (v) -> true));

        assertTrue(CollectionUtil.allMatch(Arrays.asList(), (v) -> true));
    }

    @Test
    public void testReduce() {
        assertEquals(CollectionUtil.reduce(Arrays.asList(1,2,3,4,5), (a, b) -> a + b), 15);
        assertEquals(CollectionUtil.reduce(Arrays.asList(1,2,3,4,5), (a, b) -> a * b), 120);

        assertNull(CollectionUtil.reduce(new ArrayList<Integer>(0), (a, b) -> a * b));
    }

    @Test
    public void testProduct() {
        assertEquals(CollectionUtil.product(Arrays.asList(1,2,3,4,5)), 120);

        assertEquals(CollectionUtil.product(Arrays.asList()), 0);
    }

    @Test
    public void testSum() {
        assertEquals(CollectionUtil.sum(Arrays.asList(1,2,3,4,5)), 15);
    }
}
