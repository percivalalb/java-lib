/**
 * Java Library - Useful utility function
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package com.alexbarter.math;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ComplexNumber {

    public static final ComplexNumber ZERO = ComplexNumber.from(0, 0, 0, 0);
    public static final ComplexNumber ONE = ComplexNumber.from(1, 0, 1, 0);
    public static final ComplexNumber i = ComplexNumber.from(0, 1, 1, Math.PI / 2);
    public static final ComplexNumber e = ComplexNumber.from(Math.E, 0, Math.E, 0);
    public static final ComplexNumber PI = ComplexNumber.from(Math.PI, 0, Math.PI, 0);

    private static double TOLERANCE = Math.pow(10, -11);

    private Double x;
    private Double y;

    private Double r;
    private Double arg;

    private ComplexNumber() {
    }

    public double modSquared() {
        return this.mod() * this.mod();
    }

    public double mod() {
        return this.getRadius();
    }

    public ComplexNumber reciprocal() {
        return ComplexNumber.fromPolar(1 / this.getRadius(), - this.getArg());
    }

    public ComplexNumber inverse() {
        return this.reciprocal();
    }

    /**
     * For a complex number x + iy returns x - iy
     * Equivalent of a reflection in the real axis
     *
     * @return The complex conjugate of this complex number
     */
    public ComplexNumber conjugate() {
        return ComplexNumber.fromCart(this.getRealPart(), -this.getImaginaryPart());
    }

    public ComplexNumber unit() {
        if (this.isZero()) {
            throw new IllegalArgumentException("Zero does not have a unit vector.");
        }

        return ComplexNumber.fromPolar(1, this.getArg());
    }


    public ComplexNumber minus() {
        return ComplexNumber.fromCart(-this.getRealPart(), -this.getImaginaryPart());
    }

    /**
     * For a complex number (x + iy) + (u + iv) returns (x + u) + i(y + v)
     *
     * @return The sum of this complex number and the given one
     */
    public ComplexNumber add(ComplexNumber other) {
        return this.add(other.getRealPart(), other.getImaginaryPart());
    }

    public ComplexNumber add(double xIn, double yIn) {
        return ComplexNumber.fromCart(this.getRealPart() + xIn, this.getImaginaryPart() + yIn);
    }

    public ComplexNumber subtract(ComplexNumber other) {
        return this.subtract(other.getRealPart(), other.getImaginaryPart());
    }

    public ComplexNumber subtract(double xIn, double yIn) {
        return ComplexNumber.fromCart(this.getRealPart() - xIn, this.getImaginaryPart() - yIn);
    }

    public ComplexNumber multiply(double constant) {
        return this.multiply(constant, 0);
    }

    public ComplexNumber multiply(ComplexNumber other) {
        return this.multiply(other.getRealPart(), other.getImaginaryPart());
    }


    public ComplexNumber multiply(double xIn, double yIn) {
        return ComplexNumber.fromCart(this.getRealPart() * xIn - this.getImaginaryPart() * yIn, this.getRealPart() * yIn + xIn * this.getImaginaryPart());
    }

    public ComplexNumber divide(ComplexNumber other) {
        return this.multiply(other.conjugate()).divide(other.modSquared());
    }

    public ComplexNumber divide(double constant) {
        return ComplexNumber.fromCart(this.getRealPart() / constant, this.getImaginaryPart() / constant);
    }

    public ComplexNumber power(ComplexNumber powerIn) {
        ComplexNumber power = powerIn.multiply(this.log());
        return ComplexNumber.fromPolar(Math.pow(Math.E, power.getRealPart()), power.getImaginaryPart());
    }

    public ComplexNumber power(double powerIn) {
        return ComplexNumber.fromPolar(Math.pow(this.getRadius(), powerIn), this.getArg() * powerIn);
    }

    public List<ComplexNumber> nthRoots(int n) {
        if (n < 1) {
            throw new IllegalArgumentException("nth-roots of a complex number does not make sense for n < 1.");
        }

        double size = Math.pow(this.getRadius(), 1D / n);

        List<ComplexNumber> roots = new ArrayList<>(n);

        for (int k = 0; k < n; k++) {
            roots.add(ComplexNumber.fromPolar(size, (this.getArg() + 2 * Math.PI * k) / n));
        }

        return roots;
    }

    /**
     *
     * @return The principle branch of the logarithm
     */
    public ComplexNumber log() {
        int n = 0;
        double arg = this.getArg() + 2 * n * Math.PI;
        return ComplexNumber.fromCart(Math.log(this.getRadius()), arg);
    }

    public boolean isReal() {
        return this.getImaginaryPart() == 0;
    }

    public boolean isZero() {
        return this.equals(ZERO);
    }

    public double getRealPart() {
        if (this.x == null) {
            this.x = this.r * Math.cos(this.arg);
        }

        return this.x;
    }

    public double getImaginaryPart() {
        if (this.y == null) {
            this.y = this.r * Math.sin(this.arg);
        }

        return this.y;
    }

    public double getArg() {
        if (this.arg == null) {
            this.arg = Math.atan2(this.y, this.x);
        }

        return this.arg;
    }

    public double getRadius() {
        if (this.r == null) {
            this.r = Math.sqrt(this.getRealPart() * this.getRealPart() + this.getImaginaryPart() * this.getImaginaryPart());
        }

        return this.r;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getRealPart(), this.getImaginaryPart());
    }

    @Override
    public String toString() {
        if (this.getImaginaryPart() < 0) {
            return String.format("%f - i%f", this.getRealPart(), -this.getImaginaryPart());
        } else {
            return String.format("%f + i%f", this.getRealPart(), this.getImaginaryPart());
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != this.getClass()) {
            return false;
        } else if (!(obj instanceof ComplexNumber)) {
            return false;
        }

        ComplexNumber other = (ComplexNumber) obj;

        return this.subtract(other).mod() < TOLERANCE;
    }

    public final static double ensureArgIsPrinciple(double arg) {
        while (arg > Math.PI) {
            arg -= 2 * Math.PI;
        }

        while (arg <= -Math.PI) {
            arg += 2 * Math.PI;
        }

        return arg;
    }

    public static ComplexNumber real(double realNum) {
        return from(realNum, 0D, Math.abs(realNum), realNum >= 0 ? 0 : Math.PI);
    }

    public static ComplexNumber imag(double imagPart) {
        return from(0D, imagPart, Math.abs(imagPart), imagPart == 0 ? 0 : Math.PI - Math.PI * Math.signum(imagPart));
    }

    public static ComplexNumber fromCart(double x, double y) {
        ComplexNumber cn = new ComplexNumber();
        cn.x = x;
        cn.y = y;
        return cn;
    }

    public static ComplexNumber fromPolar(double radius, double theta) {
        if (radius < 0) {
            throw new IllegalArgumentException("Radius of a complex number can not be negative.");
        }

        ComplexNumber cn = new ComplexNumber();
        cn.r = radius;
        cn.arg = ensureArgIsPrinciple(theta);
        return cn;
    }

    private static ComplexNumber from(double x, double y, double radius, double theta) {
        ComplexNumber cn = fromPolar(radius, theta);
        cn.x = x;
        cn.y = y;
        return cn;
    }
}
