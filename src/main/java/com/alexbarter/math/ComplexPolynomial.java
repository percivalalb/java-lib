/**
 * Java Library - Useful utility function
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package com.alexbarter.math;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class ComplexPolynomial {

    private Map<Long, ComplexNumber> staticCoefficents = new HashMap<>();
    private Function<Long, ComplexNumber> coefficents = (k) -> null;
    private long min = Long.MAX_VALUE, max = Long.MIN_VALUE;

    public ComplexPolynomial() {
    }

    public ComplexPolynomial(ComplexNumber... cns) {
        this.setCoefficent(0, cns);
    }

    public void setCoefficent(long start, ComplexNumber... cns) {
        for (int i = 0; i < cns.length; i++) {
            this.setCoefficent(i + start, cns[i]);
        }
    }

    public void setCoefficent(long k, ComplexNumber c) {
        this.staticCoefficents.put(k, c);
        this.min = Math.min(this.min, k);
        this.max = Math.max(this.max, k);

    }

    public void setNegative(int min, Function<Long, ComplexNumber> func) {
        this.setCoefficent(-min, -1, (k) -> k < 0 ? func.apply(k) : null);
    }

    public void setNonNegative(int max, Function<Long, ComplexNumber> func) {
        this.setCoefficent(0, max, (k) -> k >= 0 ? func.apply(k) : null);
    }

    public void setCoefficent(int min, int max, Function<Long, ComplexNumber> func) {
        Function<Long, ComplexNumber> previous = this.coefficents;
        this.coefficents = (k) -> {
            ComplexNumber coefficent = func.apply(k);
            return coefficent != null ? coefficent : previous.apply(k);
        };

        //TODO Constrict to non-zero terms
        this.min = Math.min(this.min, min);
        this.max = Math.max(this.max, max);
    }

    public ComplexNumber getCoefficent(long k) {
        ComplexNumber staticCN = this.staticCoefficents.get(k);
        if (staticCN != null) {
            return staticCN;
        }

        ComplexNumber coefficent = this.coefficents.apply(k);

        return coefficent != null ? coefficent : ComplexNumber.ZERO;
    }

    public ComplexNumber evaluate(ComplexNumber z) {
        ComplexNumber total = ComplexNumber.ZERO;

        for (long k = this.min; k <= this.max; k++) {
            ComplexNumber a = this.getCoefficent(k);
            total = total.add(a.multiply(z.power(k)));
        }

        return total;
    }

    public ComplexPolynomial multiply(ComplexPolynomial other) {
        ComplexPolynomial poly = new ComplexPolynomial();
        long min = this.min + other.min;
        long max = this.max + other.max;

        poly.coefficents = (k) -> {
            if (k > max || k < min) { return null; }
            ComplexNumber a = ComplexNumber.ZERO;

            for (long x = this.min; x <= this.max; x++) {
                long y = k - x;

                if (y > other.max || y < other.min) {
                    continue;
                }

                a = a.add(this.getCoefficent(x).multiply(other.getCoefficent(y)));
            }

            return a;
        };

        poly.min = min;
        poly.max = max;

        return poly;
    }

//    @Override
//    public int hashCode() {
//        return Objects.hash();
//    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        boolean flag = false;
        for (long k = this.min; k <= this.max; k++) {
            ComplexNumber a = this.getCoefficent(k);

            if (flag) {
                builder.append(" + ");
            }

            if (a.isZero()) {
                continue;
            } else if (a.isReal()) {
                builder.append(a.getRealPart());
            } else {
                builder.append(a);
            }

            builder.append("x^");
            builder.append(k);
            flag = true;
        }

        return builder.toString();

    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != this.getClass()) {
            return false;
        } else if (!(obj instanceof ComplexPolynomial)) {
            return false;
        }

        ComplexPolynomial other = (ComplexPolynomial) obj;

        if (other.min != this.min || other.max != this.max) {
            return false;
        }

        for (long k = this.min; k <= this.max; k++) {
            if (!this.getCoefficent(k).equals(other.getCoefficent(k))) {
                return false;
            }
        }

        return true;
    }
}
