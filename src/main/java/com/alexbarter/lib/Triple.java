/**
 * Java Library - Useful utility function
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package com.alexbarter.lib;

import java.util.Objects;

/**
*
* @author Alex Barter
*
* @param <L> The generic of the left object
* @param <M> The generic of the middle object
* @param <R> The generic of the right object
*/
public class Triple<L, M, R> {

    private final L left;
    private final M middle;
    private final R right;

    public Triple(L leftIn, M middleIn, R rightIn) {
        this.left = leftIn;
        this.middle = middleIn;
        this.right = rightIn;
    }

    public L getLeft() {
        return this.left;
    }

    public M getMiddle() {
        return this.middle;
    }

    public R getRight() {
        return this.right;
    }

    public boolean leftEquals(Triple other) {
        return Objects.equals(this.left, other.getLeft());
    }

    public boolean middleEquals(Triple other) {
        return Objects.equals(this.middle, other.getMiddle());
    }

    public boolean rightEquals(Triple other) {
        return Objects.equals(this.right, other.getRight());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (!(obj instanceof Triple)) {
            return false;
        } else {
            Triple<?, ?, ?> other = (Triple<?, ?, ?>) obj;

            return Objects.equals(this.left, other.getLeft()) && Objects.equals(this.middle, other.getMiddle()) && Objects.equals(this.right, other.getRight());
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.left, this.middle, this.right);
    }

    @Override
    public String toString() {
        return "Triple [left=" + this.left + ", middle=" + this.middle + ", right=" + this.right + "]";
    }

    public static <L, M, R> Triple<L, M, R> of(L left, M middle, R right) {
        return new Triple<>(left, middle, right);
    }
}
