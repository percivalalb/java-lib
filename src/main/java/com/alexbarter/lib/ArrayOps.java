package com.alexbarter.lib;

import java.lang.reflect.Array;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * import static com.alexbarter.lib.ArrayOps.map;
 */
public final class ArrayOps {

    private ArrayOps() {}

    public static <T> T[] map(BiFunction<T, Integer, T> mapper, T[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = mapper.apply(array[i], i);
        }
        return array;
    }

    public static <T> T[] map(Function<T, T> mapper, T[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = mapper.apply(array[i]);
        }
        return array;
    }

    public static <T> T[] filter(BiPredicate<T, Integer> keep, T[] array) {
        T[] first = (T[]) Array.newInstance(array.getClass().getComponentType(), array.length);

        int j = 0;
        for (int i = 0; i < array.length; i++) {
            if (keep.test(array[i], i)) {
                first[j++] = array[i];
            }
        }

        return trim(first, j);
    }

    public static <T> T[] filter(Predicate<T> keep, T[] array) {
        T[] first = (T[]) Array.newInstance(array.getClass().getComponentType(), array.length);

        int j = 0;
        for (int i = 0; i < array.length; i++) {
            if (keep.test(array[i])) {
                first[j++] = array[i];
            }
        }

        return trim(first, j);
    }

    /**
     * Returns the first n elements of the array
     */
    public static <T> T[] first(T[] array, final int n) {
        int len = Math.min(n, array.length);
        T[] first = (T[]) Array.newInstance(array.getClass().getComponentType(), len);
        System.arraycopy(array, 0, first, 0, len);
        return first;
    }

    /**
     * Returns the last n elements of the array
     */
    public static <T> T[] last(T[] array, final int n) {
        int len = Math.min(n, array.length);
        T[] first = (T[]) Array.newInstance(array.getClass().getComponentType(), len);
        System.arraycopy(array, array.length - len, first, 0, len);
        return first;
    }

    public static <T> T[] trim(T[] array, int len) {
        T[] trimmed = (T[]) Array.newInstance(array.getClass().getComponentType(), len);
        System.arraycopy(array, 0, trimmed, 0, len);
        return trimmed;
    }
}
