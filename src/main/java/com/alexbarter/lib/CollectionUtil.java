/**
 * Java Library - Useful utility function
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package com.alexbarter.lib;

import java.util.Iterator;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Predicate;

/**
 * Provides similar methods to that off {@link java.util.stream.Stream} but
 * without the overhead of creating a stream.
 */
public class CollectionUtil {

    public static <T> boolean allMatch(Iterable<T> collection, Predicate<T> pred) {
        Objects.requireNonNull(pred);
        for (T e : collection) {
            if (!pred.test(e)) {
                return false;
            }
        }
        return true;
    }

    public static <T> boolean noneMatch(Iterable<T> collection, Predicate<T> pred) {
        Objects.requireNonNull(pred);
        for (T e : collection) {
            if (pred.test(e)) {
                return false;
            }
        }
        return true;
    }

    public static <T> boolean anyMatch(Iterable<T> collection, Predicate<T> pred) {
        Objects.requireNonNull(pred);
        for (T e : collection) {
            if (pred.test(e)) {
                return true;
            }
        }
        return false;
    }

    // Exactly one match
    public static <T> boolean oneMatch(Iterable<T> collection, Predicate<T> pred) {
        Objects.requireNonNull(pred);
        boolean match = false;
        for (T e : collection) {
            if (match && pred.test(e)) {
                return false;
            } else {
                match = pred.test(e);
            }
        }
        return match;
    }

    public static Integer sum(Iterable<Integer> collection) {
        return sum(0, collection);
    }

    public static Integer product(Iterable<Integer> collection) {
        return product(0, collection);
    }

    public static Integer sum(Integer identity, Iterable<Integer> collection) {
        return reduce(collection, (a, b) -> a + b, identity);
    }

    public static Integer product(Integer identity, Iterable<Integer> collection) {
        return reduce(collection, (a, b) -> a * b, identity);
    }

    /**
     * Applies one value with the next and applies that value to the next etc
     * till the end of the list is reached and a final value is produced which
     * is then returned.
     *
     * Returns null if no values are given.
     *
     * @param collection The collection of values to reduce
     * @param func The reduction function
     * //@param _default The value returned when there are no values
     * @param <T> The type for the the operations
     * @return The combined value/object for the reduction of the all the inputs
     */
    public static <T> T reduce(Iterable<T> collection, BiFunction<T, T, T> func) {
        return reduce(collection, func, null);
    }

    public static <T> T reduce(Iterable<T> collection, BiFunction<T, T, T> func, T _default) {
        Objects.requireNonNull(collection);
        Objects.requireNonNull(func);

        Iterator<T> it = collection.iterator();
        T prev = it.hasNext() ? it.next() : _default;
        while (it.hasNext()) {
            prev = func.apply(prev, it.next());
        }

        return prev;
    }
}
