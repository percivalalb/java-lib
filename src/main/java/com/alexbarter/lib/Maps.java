/**
 * Java Library - Useful utility function
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package com.alexbarter.lib;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class Maps {

    private final Map<String, Object> map;

    private Maps(Supplier<Map<String, Object>> map) {
        this.map = map.get();
    }

    public Maps put(String key, Object value) {
        this.map.put(key, value);
        return this;
    }

    public Map<String, Object> get() {
        return this.map;
    }

    public static Maps of(Object... args) {
        return of(HashMap::new, args);
    }

    public static Maps of(Supplier<Map<String, Object>> map, Object... args) {
        if (args.length % 2 != 0) {
            throw new IllegalArgumentException();
        }

        Maps maps = new Maps(map);

        for (int i = 0; i < args.length; i+=2) {
            String key = args[i].toString();
            maps.put(key, args[i + 1]);
        }

        return maps;
    }
}
