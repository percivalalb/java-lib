/**
 * Java Library - Useful utility function
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package com.alexbarter.lib;

import java.util.Objects;

/**
 *
 * @author Alex Barter
 *
 * @param <L> The generic of the left object
 * @param <R> The generic of the right object
 */
public class Pair<L, R> {


    private final L left;
    private final R right;

    public Pair(L leftIn, R rightIn) {
        this.left = leftIn;
        this.right = rightIn;
    }

    public L getLeft() {
        return this.left;
    }

    public R getRight() {
        return this.right;
    }

    public boolean leftEquals(Pair<L, ?> other) {
        return Objects.equals(this.left, other.getLeft());
    }

    public boolean rightEquals(Pair<?, R> other) {
        return Objects.equals(this.right, other.getRight());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (!(obj instanceof Pair)) {
            return false;
        } else {
            Pair<?, ?> other = (Pair<?, ?>) obj;

            return Objects.equals(this.left, other.getLeft()) && Objects.equals(this.right, other.getRight());
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.left, this.right);
    }

    @Override
    public String toString() {
        return "Pair [left=" + this.left + ", right=" + this.right + "]";
    }

    public static <L, R> Pair<L, R> of(L left, R right) {
        return new Pair<>(left, right);
    }
}
